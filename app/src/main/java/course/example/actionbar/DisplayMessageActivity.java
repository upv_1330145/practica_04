package course.example.actionbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        TextView textview = (TextView)findViewById(R.id.textView1);
        Intent intent = getIntent();
        String total = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        textview.setText(total);
    }
}
